import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        loadComponent: () => import('../pages/base/base.page').then(m => m.BasePage)
    }
];
