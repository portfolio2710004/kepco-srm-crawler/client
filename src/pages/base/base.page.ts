import { CommonModule } from "@angular/common";
import { Component, NgZone } from "@angular/core";

import { tap } from "rxjs";

import { AnnouncementApi, TAddAnnouncement } from "src/shared";

@Component({
    selector: 'app-base',
    templateUrl: './base.page.html',
    standalone: true,
    imports: [
        CommonModule
    ]
})
export class BasePage {

    announcements: TAddAnnouncement[] = [];
    
    refreshProcessMessage = '';

    constructor(
        private readonly announcementApi: AnnouncementApi,
        private readonly zone: NgZone
    ) {
        this.announcementApi.getAnnouncements().pipe(
            tap((announcements: TAddAnnouncement[]) => this.announcements = announcements)
        ).subscribe();
    }
    
    onRefresh() {
        this.refreshProcessMessage = '준비중';

        this.zone.runOutsideAngular(() => {
            this.announcementApi.refreshProcess().pipe(
                tap((message: string) => {
                    this.zone.run(() => {
                        
                        this.refreshProcessMessage = message;

                        if (message === 'DONE') {
                            this.refreshProcessMessage = '';
                            this.announcementApi.refreshProcessClose();
                            this.announcementApi.getAnnouncements().pipe(
                                tap((announcements: TAddAnnouncement[]) => this.announcements = announcements)
                            ).subscribe();
                        }
                    });
                })
            ).subscribe();
        });
    }
}   