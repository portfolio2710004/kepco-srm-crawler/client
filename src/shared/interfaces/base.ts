export type EntityDates = {
    createdAt: Date;
    updatedAt: Date;
}