import { TAddAnnouncement } from "../announcement/announcement";

export interface ICrawlingService {
    execute(): Promise<TAddAnnouncement[]>;
}

export const CRAWLING_SERVICE = 'CRAWLING_SERVICE';