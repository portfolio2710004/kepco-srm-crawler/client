import { EntityDates } from "../base";

// 공고 도메인 모델
export type TAnnouncement = {
    id: string; // ID
    name: string; // 공고명
    department: string; // 공고 부서
    bidType: string; // 입찰 종류 
    bidMethod: string; // 입찰 방법
    bidderSelection: string; // 낙찰자 선정방법
    bidApplicationAt: string; // 입찰 신청일시
    biddingStartAt: string; // 투찰 시작일시
    biddingEndAt: string; // 투찰 마감일시
    announcementDate: Date; // 공고 일자
    progressStatus: string; // 진행상태
    regionLimit: string; // 지역제한
    estimatedPrice: string; // 추정가격
} & EntityDates;

export type TAddAnnouncement = Omit<TAnnouncement, 'createdAt' | 'updatedAt'>;

export type TAddAnnouncementStrategy = 'today'|'yesterday';