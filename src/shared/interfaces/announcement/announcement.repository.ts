import { TAddAnnouncement, TAnnouncement } from "./announcement";

export interface IAnnouncementRepository {
    exists(ids: string[]): Promise<boolean>;
    findAnnouncements(): Promise<TAddAnnouncement[]>;
    creates(announcements: TAddAnnouncement[]): Promise<TAnnouncement[]>;
}

export const ANNOUNCE_REPOSITORY = 'ANNOUNCE_REPOSITORY';