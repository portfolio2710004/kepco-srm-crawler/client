import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { nanoid } from 'nanoid';

import { Observable } from "rxjs";

import { TAddAnnouncement } from "src/shared";

import { env } from "../env/env.development";

@Injectable({
    providedIn: 'root'
})
export class AnnouncementApi {

    private eventSource: EventSource | null = null;

    constructor(
        private readonly httpClient: HttpClient
    ) { }

    getAnnouncements() {
        return this.httpClient.get<TAddAnnouncement[]>(`${env.apiUrl}/api/announcements`);
    }
    
    refreshProcess(): Observable<string> {
        return new Observable(observer => {
            this.eventSource = new EventSource(`${env.apiUrl}/api/announcements/${nanoid()}/process`);

            this.eventSource.onmessage = event => {
                try {
                    const message = event.data;

                    observer.next(message);
                } catch (error) {
                    console.error('Error parsing sensor data', error);
                    observer.error(error);
                }
            };

            this.eventSource.onerror = error => {
                observer.error(error);
            };
        });
    }

    refreshProcessClose() {
        this.eventSource?.close();
    }
}